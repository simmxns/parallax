const stars: HTMLElement = document.getElementById('stars')
const moon: HTMLElement = document.getElementById('moon')
const mountains_behind: HTMLElement = document.getElementById(
	'mountains_behind'
)
const mountains_front: HTMLElement = document.getElementById('mountains_front')
const text: HTMLElement = document.getElementById('text')
const btn: HTMLElement = document.getElementById('btn')
const header: HTMLElement = document.querySelector('header')

window.addEventListener('scroll', (): void => {
	var value: number = window.scrollY
	stars.style.left = value * 0.25 + 'px'
	moon.style.top = value * 1.05 + 'px'
	mountains_behind.style.top = value * 0.5 + 'px'
	mountains_front.style.top = value * 0 + 'px'
	text.style.marginRight = value * 4 + 'px'
	text.style.marginTop = value * 1.5 + 'px'
	btn.style.marginTop = value * 1.5 + 'px'
	header.style.top = value * .5 + 'px'
})
